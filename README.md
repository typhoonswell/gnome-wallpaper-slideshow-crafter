# Gnome Wallpaper Slideshow Crafter

![](doc/gwsc-ss.png)

## Background

For some reason the Gnome 3 Desktop Environment has the ability to render Wallpaper Slideshows, but gives the user no mechanism to create one themselves. This project aims to bridge this gap by making it possible to create and edit your own Wallpaper Slideshows, and export them to Gnome.

## Dependencies

ruby >= 2.7

gems:
* nokogiri
* gtk3

(gtk3 gem may require `rake` during install)

The script must be run in an environment with the `$HOME` environment variable correctly set.

## Instructions

The app can be run by executing the `gwsc` script after installing the dependencies. Use the controls to construct your slideshow by alternating wallpapers and transitions with the desired durations for each. When you're finished, be sure to set the Name field, which will be used to register the project with the Gnome Wallpaper Registry. Finally, click the `Export` button on the title bar. Your new Wallpaper Slideshow should be visible in the Gnome `Settings > Background` menu. 

## How It Works

You can create and edit Wallpaper Slideshow documents using this tool, but you must `Export` for it to be made available to the Gnome subsystems. This `Export` function follows these steps:

1. Creates the `$HOME/Pictures/Wallpapers` directory if it does not already exist.
2. Creates the `$HOME/.local/share/gnome-background-properties` directory if it does not already exist.
3. Generates and saves the Wallpaper Slideshow file in the `$HOME/Pictures/Wallpapers` directory.
4. Either creates or adds an entry to the `$HOME/.local/share/gnome-background-properties/gnome-backgrounds.xml` file, referencing the Slideshow file we just created.

These steps are required for Gnome to pick up these changes and make it selectable from the `Settings > Background` menu.

## Known Issues

* There's no way to delete an entry from the Gnome Wallpaper Registry without manually editing the XML file.
* I'm new to Ruby so I'm sure the source is amateurish.
* Images that are not 16:9 ratio may not render correctly in the list view.
* Code needs a lot of cleanup and documentation.
* Needs a custom icon.
* Might be best to save the thumbnails for caching.

## Authors

Tony McKeehan