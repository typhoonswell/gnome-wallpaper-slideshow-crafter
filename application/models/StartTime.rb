module GWSC
  class StartTime
    PROPERTIES = %i[year month day hour minute second].freeze
    attr_accessor *PROPERTIES

    def initialize(options = {})
      @year = options[:year] || '1970'
      @month = options[:month] || '1'
      @day = options[:day] || '1'
      @hour = options[:hour] || '0'
      @minute = options[:minute] || '00'
      @second = options[:second] || '00'
    end

    def to_s
      super

      "#{hour}:#{minute}:#{second} #{month}/#{day}/#{year}"
    end
  end
end
