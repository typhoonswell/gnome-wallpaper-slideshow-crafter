module GWSC
  class SelectionItem
    PROPERTIES = %w[type duration file iter].freeze

    attr_accessor *PROPERTIES

    def initialize(type, options = {})
      raise ArgumentError if options[:duration].nil?

      @duration = options[:duration]
      @type = type
      @iter = options[:iter] unless options[:iter].nil?

      case type
      when GWSC::TRANSITION
        # nop
      when GWSC::WALLPAPER
        @file = options[:file]
      else
        raise ArgumentError
      end
    end
  end
end
