module GWSC
  class Context
    PROPERTIES = %w[application_base_dir].freeze

    attr_reader *PROPERTIES

    def initialize(application_base_dir)
      @application_base_dir = application_base_dir
    end
  end
end
