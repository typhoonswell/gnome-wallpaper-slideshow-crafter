require 'nokogiri'

module GWSC
  class Background
    attr_accessor :starttime, :items, :adjustment, :name, :save_path

    def initialize(options = {})
      @items = []
      @starttime = StartTime.new hour: '8'
      @adjustment = 'Zoom'
      @name = options[:name]
      @save_path = options[:save_path]
      unless options[:input].nil?
        items = options[:input].at_xpath('/background')
        items.element_children.each do |elem|
          case elem.node_name.downcase
          when 'starttime'
            #noop
          when 'transition'
            @items << SelectionItem.new(
              GWSC::TRANSITION,
              duration: elem.xpath('duration').text.to_i
            )
          when 'static'
            @items << SelectionItem.new(
              GWSC::WALLPAPER,
              duration: elem.xpath('duration').text.to_i,
              file: elem.xpath('file').text
            )
          end
        end
      end
    end

    def to_x
      first_static = nil
      last_static = nil

      export_items = condense_transitions @items
      export_items = move_starting_transition_to_end export_items

      b = Nokogiri::XML::Builder.new do |xml|
        xml.background {
          xml.starttime {
            xml.year @starttime.year
            xml.month @starttime.month
            xml.day @starttime.day
            xml.hour @starttime.hour
            xml.minute @starttime.minute
            xml.second @starttime.second
          }
          export_items.each_with_index do |item, index|
            case item.type
            when GWSC::WALLPAPER
              first_static = item.file if first_static.nil?
              last_static = item.file
              xml.send('static') {
                xml.duration item.duration.to_f
                xml.file item.file
              }
            when GWSC::TRANSITION
              from = last_static
              to = if index.eql? export_items.length - 1
                     first_static
                   else
                     export_items[index + 1].file
                   end
              xml.send('transition') {
                xml.duration item.duration.to_f
                xml.from from
                xml.to to
              }
            else
              raise IndexError "#{item.type} item type not supported"
            end
          end
        }
      end

      b.to_xml
    end

    def condense_transitions(items)
      export_items = []
      current_transition = nil
      items.each do |item|
        case item.type
        when GWSC::WALLPAPER
          export_items << current_transition unless current_transition.nil?
          export_items << item
          current_transition = nil
        when GWSC::TRANSITION
          if current_transition.nil?
            current_transition = item
          else
            current_transition.duration += item.duration
          end
        end
      end
      export_items << current_transition unless current_transition.nil?

      export_items
    end

    def move_starting_transition_to_end(items)
      unless items.nil? || items.length < 2
        if items[0].type == GWSC::TRANSITION
          if items[items.length - 1].type == GWSC::TRANSITION
            items[items.length - 1].duration += items[0].duration
          else
            items << items[0]
          end
          items.delete_at(0)
        end
      end

      items
    end
  end
end
