# frozen_string_literal: true

module GWSC
  class HumanReadable
    def self.secs_to_human(secs)
      [[60, :seconds], [60, :minutes], [24, :hours], [Float::INFINITY, :days]].map do |count, name|
        next unless secs.positive?

        secs, n = secs.divmod(count)

        "#{n.to_i} #{name}" unless n.zero?
      end.compact.reverse.join(' ')
    end
  end
end
