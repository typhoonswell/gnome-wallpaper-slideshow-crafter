module GWSC
  class FileProcedureHelpers
    LOCAL_REGISTRY_DIR = ENV['HOME'] + '/.local/share/gnome-background-properties'.freeze
    LOCAL_REGISTRY_PATH = ENV['HOME'] + '/.local/share/gnome-background-properties/gnome-backgrounds.xml'.freeze
    LOCAL_DEFINITION_DIR = ENV['HOME'] + '/Pictures/Wallpapers'.freeze
    LOCAL_DEFINITION_FORMAT = ENV['HOME'] + '/Pictures/Wallpapers/%s.xml'.freeze

    def self.export(name, xml, adjustment)
      name = name.strip
      # check if $HOME/Pictures/Wallpapers exists, creating if not
      create_dir_recursively LOCAL_DEFINITION_DIR unless check_local_definition

      # save the file in a special place
      filepath = LOCAL_DEFINITION_FORMAT % name.gsub(' ', '_')
      save_definition(filepath, xml)

      registry = nil
      # check if $HOME/.local/share/gnome-background-properties/gnome-backgrounds.xml exists
      if check_registry
        registry = Nokogiri::XML(File.open(LOCAL_REGISTRY_PATH)) do |config|
          config.default_xml.noblanks
        end

        # check if there's an entry already in the registry
        wallpaper = registry.at_xpath("/wallpapers/wallpaper[name = '#{name}']")
        if wallpaper.nil?
          add_registry_entry(registry.at_xpath('/wallpapers'), name, filepath, adjustment)
        else
          wallpaper.at_xpath('./filename').content = filepath
          wallpaper.at_xpath('./options').content = adjustment.downcase
        end
      else
        # if not, create it and seed the first value, using the full name
        create_dir_recursively LOCAL_REGISTRY_DIR unless File.directory?(LOCAL_REGISTRY_DIR)
        registry = Nokogiri::XML::Builder.new do |xml|
          xml.doc.create_internal_subset(
              'wallpapers',
              'SYSTEM',
              'gnome-wp-list.dtd'
          )
          xml.wallpapers {
            xml.wallpaper('deleted' => 'false') {
              xml.name name
              xml.filename filepath
              xml.options adjustment.downcase
            }
          }
        end
      end

      save_registry(registry.to_xml(indent: 4))
    end

    def self.save_registry(registry_xml)
      File.open(LOCAL_REGISTRY_PATH, 'w') do |f|
        f.write registry_xml
        f.close
      end
    end

    def self.save_definition(filepath, registry_xml)
      File.open(filepath, 'w') do |f|
        f.write registry_xml
        f.close
      end
    end

    def self.get_registered_name(open_path)
      # check if open_path is $HOME/Pictures/Wallpapers/*.xml format
      expected_filepath = LOCAL_DEFINITION_FORMAT % File.basename(open_path, File.extname(open_path))
      return nil unless /#{expected_filepath}/ =~ open_path

      # check if $HOME/.local/share/gnome-background-properties/gnome-backgrounds.xml exists
      return nil unless check_registry

      # check registry for entry with filename element value equal to open_path
      registry = Nokogiri::XML File.open(LOCAL_REGISTRY_PATH)
      # if none, return nil. if yes, return name element value

      wallpaper = registry.at_xpath("/wallpapers/wallpaper[filename = '#{open_path}']")
      return nil if wallpaper.nil?

      wallpaper.xpath('./name').text
    end

    def self.add_registry_entry(wallpapers_node, name, filepath, adjustment)
      wallpaper_node = Nokogiri::XML::Node.new 'wallpaper', wallpapers_node
      wallpaper_node['deleted'] = 'false'
      name_node = Nokogiri::XML::Node.new 'name', wallpaper_node
      name_node.inner_html = name
      filename_node = Nokogiri::XML::Node.new 'filename', wallpaper_node
      filename_node.inner_html = filepath
      options_node = Nokogiri::XML::Node.new 'options', wallpaper_node
      options_node.inner_html = adjustment.downcase

      wallpaper_node.add_child(name_node)
      wallpaper_node.add_child(filename_node)
      wallpaper_node.add_child(options_node)
      wallpapers_node.add_child(wallpaper_node)
    end

    def self.check_home_envvar
      raise ScriptError '$HOME env var required to handle local Gnome wallpaper registry.' if ENV['HOME'].nil?
    end

    def self.check_registry
      check_home_envvar
      (File.directory?(LOCAL_REGISTRY_DIR) && File.file?(LOCAL_REGISTRY_PATH))
    end

    def self.check_local_definition
      check_home_envvar
      File.directory?(LOCAL_DEFINITION_DIR)
    end

    def self.create_dir_recursively(dir)
      FileUtils.mkdir_p(dir)
    end
  end
end
