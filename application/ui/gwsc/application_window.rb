# frozen_string_literal: true

require 'gtk3'

module GWSC
  class ApplicationWindow < Gtk::ApplicationWindow
    attr_accessor :bg, :gnome_wallpaper_xsd, :model
    COL_POS, COL_TYPE, COL_FILE, COL_DURATION = 0, 1, 2, 3
    WALLPAPER = 'Wallpaper'
    TRANSITION = 'Transition'
    UNIT_CONV = { 'seconds' => 1.0, 'minutes' => 60.0, 'hours' => 3600.0, 'days' => 86_400.0 }.freeze
    type_register

    class << self
      def init
        set_template resource: '/com/typhoonswell/gnome-wallpaper-slideshow-crafter/ui/application_window.ui'

        bind_template_child 'newDocButton'
        bind_template_child 'openDocButton'
        bind_template_child 'saveButton'
        bind_template_child 'saveAsButton'
        bind_template_child 'treeItemSelection'
        bind_template_child 'mainItemTree'
        bind_template_child 'imageFileChooser'
        bind_template_child 'moveUpButton'
        bind_template_child 'moveDownButton'
        bind_template_child 'addWallpaperButton'
        bind_template_child 'addTransitionButton'
        bind_template_child 'removeItemButton'
        bind_template_child 'durationValue'
        bind_template_child 'durationUnitEntry'
        bind_template_child 'durationUnitCombo'
        bind_template_child 'projectNameEntry'
        bind_template_child 'adjustmentEntry'
        bind_template_child 'adjustmentCombo'
        bind_template_child 'exportDocButton'
      end
    end

    def initialize(application, context)
      super application: application
      set_all_sensitive(false)
      @bg = Background.new name: projectNameEntry.text
      @gnome_wallpaper_xsd =
        Nokogiri::XML::Schema(File.read(context.application_base_dir + '/resources/gnome-background.xsd'))

      @model = Gtk::ListStore.new(String, String, GdkPixbuf::Pixbuf, String)
      mainItemTree.set_model(@model)
      textRenderer = Gtk::CellRendererText.new
      mainItemTree.append_column(Gtk::TreeViewColumn.new('Pos', textRenderer, { text: COL_POS }))
      mainItemTree.append_column(Gtk::TreeViewColumn.new('Type', textRenderer, { text: COL_TYPE }))
      thumbCol = Gtk::TreeViewColumn.new('Thumbnail', Gtk::CellRendererPixbuf.new, { pixbuf: COL_FILE })
      # col.set_fixed_width(380)
      thumbCol.set_expand(true)
      mainItemTree.append_column(thumbCol)
      durationCol = Gtk::TreeViewColumn.new('Duration', textRenderer, { text: COL_DURATION })
      durationCol.set_fixed_width(140)
      # durationCol.set_expand(true)
      mainItemTree.append_column(durationCol)

      adjustmentCombo.signal_connect 'changed' do |combo|
        @bg.adjustment = adjustmentEntry.text
      end

      projectNameEntry.signal_connect 'changed' do |entry|
        @bg.name = entry.text.strip
      end

      saveButton.signal_connect 'clicked' do |button|
        @bg.save_path = get_save_as_path if @bg.save_path.nil? || @bg.save_path.length.zero?
        FileProcedureHelpers.save_definition @bg.save_path, @bg.to_x
      end

      saveAsButton.signal_connect 'clicked' do |button|
        save_path = get_save_as_path
        unless save_path.nil?
          @bg.save_path = save_path
          FileProcedureHelpers.save_definition @bg.save_path, @bg.to_x
        end
      end

      exportDocButton.signal_connect 'clicked' do |button|
        if projectNameEntry.text.nil? || (projectNameEntry.text.strip.length == 0)
          warn_missing_project_name
        else
          FileProcedureHelpers.export(@bg.name, @bg.to_x, @bg.adjustment)
          window = Gtk::MessageDialog.new(parent: self,
                                          flags: :destroy_with_parent,
                                          type: :error,
                                          buttons_type: :close,
                                          message: 'Successfully exported slideshow to Gnome')
          window.secondary_text =
            'If it doesn\'t appear in Settings>Background, you may need to Log Out/Log In.'
          window.signal_connect "response" do |dialog, _response_id|
            dialog.destroy
          end

          window.show_all
        end
      end

      newDocButton.signal_connect 'clicked' do |button|
        set_all_sensitive(false)
        projectNameEntry.text = ''
        @model.clear
        @bg = Background.new name: projectNameEntry.text
        populate_tree
      end

      addWallpaperButton.signal_connect 'clicked' do |button|
        at_pos = @bg.items.length + 1
        unless treeItemSelection.selected.nil?
          at_pos = treeItemSelection.selected[COL_POS] || '1'
          at_pos = at_pos.to_i + 1
        end

        iter = if @bg.items.length.zero? || at_pos == 1
                 @model.append
               else
                 @model.insert_after(@bg.items[at_pos - 2].iter)
               end

        iter.set_values([at_pos.to_s, WALLPAPER, nil, GWSC::HumanReadable.secs_to_human(21_600)])
        @bg.items.insert(at_pos - 1, GWSC::SelectionItem.new(GWSC::WALLPAPER, duration: 21_600, file: nil, iter: iter))

        if @bg.items.length > at_pos
          @bg.items[at_pos..-1].each do |item|
            item.iter[COL_POS] = (item.iter[COL_POS].to_i + 1).to_s
          end
        end
      end

      addTransitionButton.signal_connect 'clicked' do |button|
        at_pos = @bg.items.length + 1
        unless treeItemSelection.selected.nil?
          at_pos = treeItemSelection.selected[COL_POS] || '1'
          at_pos = at_pos.to_i + 1
        end

        iter = if @bg.items.length.zero? || at_pos == 1
                 @model.append
               else
                 @model.insert_after(@bg.items[at_pos - 2].iter)
               end
        iter.set_values([at_pos.to_s,
                         TRANSITION,
                         nil,
                         GWSC::HumanReadable.secs_to_human(5)])
        @bg.items.insert(at_pos - 1, GWSC::SelectionItem.new(GWSC::TRANSITION, duration: 5, iter: iter))

        if @bg.items.length > at_pos
          @bg.items[at_pos..-1].each do |item|
            item.iter[COL_POS] = (item.iter[COL_POS].to_i + 1).to_s
          end
        end
      end

      removeItemButton.signal_connect 'clicked' do |button|
        unless treeItemSelection.selected.nil?
          at_pos = treeItemSelection.selected[COL_POS].to_i

          item = @bg.items[at_pos - 1]
          @model.remove(item.iter)

          if @bg.items.length > 1
            @bg.items[at_pos..-1].each do |item|
              item.iter[COL_POS] = (item.iter[COL_POS].to_i - 1).to_s
            end
          end

          @bg.items.delete_at(at_pos - 1)
        end
      end

      durationUnitCombo.signal_connect 'changed' do |combo|
        unless treeItemSelection.selected.nil?
          result = (durationValue.text.to_i * (UNIT_CONV[combo.children[0].text])).to_i
          @bg.items[treeItemSelection.selected[COL_POS].to_i-1].duration = result
          treeItemSelection.selected[COL_DURATION] = GWSC::HumanReadable.secs_to_human(result)
        end
      end

      durationValue.signal_connect 'value-changed' do |entry|
        unless treeItemSelection.selected.nil?
          result = (entry.text.to_i * (UNIT_CONV[durationUnitEntry.text])).to_i
          @bg.items[treeItemSelection.selected[COL_POS].to_i-1].duration = result
          treeItemSelection.selected[COL_DURATION] = GWSC::HumanReadable.secs_to_human(result)
        end
      end

      treeItemSelection.signal_connect 'changed' do |entry|
        if entry.selected.nil? || @bg.items.length.zero?
          set_all_sensitive(false)
        else
          set_all_sensitive(true)
          item = @bg.items[entry.selected[COL_POS].to_i - 1]
          if entry.selected[COL_TYPE].eql? WALLPAPER
            imageFileChooser.set_sensitive(true)
            imageFileChooser.select_uri 'file://' + (item.file || '')
          else
            imageFileChooser.select_uri ''
            imageFileChooser.set_sensitive(false)
          end

          value = item.duration
          if value % 86_400 == 0 # does this second value divide evenly into days
            durationValue.text = (value / 86_400).to_s
            durationUnitEntry.text = 'days'
          elsif value % 3600 == 0 # ... divide into hours
            durationValue.text = (value / 3600).to_s
            durationUnitEntry.text = 'hours'
          elsif value % 60 == 0 # ... divide into minutes
            durationValue.text = (value / 60).to_s
            durationUnitEntry.text = 'minutes'
          else
            durationValue.text = value.to_s
            durationUnitEntry.text = 'seconds'
          end
        end
      end

      imageFileChooser.signal_connect 'file-set' do |fileChooser|
        @bg.items[treeItemSelection.selected[COL_POS].to_i-1].file = fileChooser.filename
        treeItemSelection.selected[COL_FILE] = GdkPixbuf::Pixbuf.new(file: fileChooser.filename, height: 135, width: 240)
      end

      moveUpButton.signal_connect 'clicked' do |button|
        if !treeItemSelection.selected.nil? and !treeItemSelection.selected[COL_POS].eql?('1')
          from_pos = treeItemSelection.selected[COL_POS].to_i - 1
          to_pos = from_pos - 1

          @model.swap(@bg.items[from_pos].iter, @bg.items[to_pos].iter)
          @bg.items[from_pos], @bg.items[to_pos] = @bg.items[to_pos], @bg.items[from_pos]
          @bg.items[from_pos].iter[COL_POS], @bg.items[to_pos].iter[COL_POS] =
            @bg.items[to_pos].iter[COL_POS], @bg.items[from_pos].iter[COL_POS]
        end
      end

      moveDownButton.signal_connect 'clicked' do |button|
        if !treeItemSelection.selected.nil? and !treeItemSelection.selected[COL_POS].eql?(@bg.items.length.to_s)
          from_pos = treeItemSelection.selected[COL_POS].to_i - 1
          to_pos = from_pos + 1

          @model.swap(@bg.items[from_pos].iter, @bg.items[to_pos].iter)
          @bg.items[from_pos], @bg.items[to_pos] = @bg.items[to_pos], @bg.items[from_pos]
          @bg.items[from_pos].iter[COL_POS], @bg.items[to_pos].iter[COL_POS] =
            @bg.items[to_pos].iter[COL_POS], @bg.items[from_pos].iter[COL_POS]
        end
      end

      openDocButton.signal_connect 'clicked' do |button|
        # TODO are you sure? dialog
        @model.clear
        dialog = Gtk::FileChooserDialog.new(title: 'Open',
                                            action: :open,
                                            parent: self,
                                            buttons: [[Gtk::Stock::OPEN, :accept],
                                                      [Gtk::Stock::CANCEL, :cancel]])
        filter_xml = Gtk::FileFilter.new
        filter_xml.name = 'XML Docs'
        filter_xml.add_pattern('*.xml')
        dialog.add_filter(filter_xml)

        filter_all = Gtk::FileFilter.new
        filter_all.name = 'All Files'
        filter_all.add_pattern('*.*')
        dialog.add_filter(filter_all)
        if dialog.run == Gtk::ResponseType::ACCEPT
          wp_xml = Nokogiri::XML(File.open(dialog.filename))
          if @gnome_wallpaper_xsd.valid? wp_xml
            registered_name = FileProcedureHelpers.get_registered_name(dialog.filename)
            projectNameEntry.text = if registered_name.nil?
                                      File.basename(dialog.filename, File.extname(dialog.filename))
                                    else
                                      registered_name
                                    end

            @bg = Background.new name: projectNameEntry.text, input: wp_xml, save_path: dialog.filename
            populate_tree
          else
            warn_invalid_slideshow_format
          end
        end
        dialog.destroy
      end
    end

    def get_save_as_path
      dialog = Gtk::FileChooserDialog.new(title: 'Save as...',
                                          action: :save,
                                          parent: self,
                                          buttons: [[Gtk::Stock::SAVE, :accept],
                                                    [Gtk::Stock::CANCEL, :cancel]])
      ret = nil
      ret = dialog.filename if dialog.run == Gtk::ResponseType::ACCEPT

      dialog.destroy
      ret
    end

    def warn_missing_project_name
      window = Gtk::MessageDialog.new(parent: self,
                                      flags: :destroy_with_parent,
                                      type: :error,
                                      buttons_type: :close,
                                      message: 'Please enter a project name before exporting.')

      window.signal_connect "response" do |dialog, _response_id|
        dialog.destroy
      end

      window.show_all
    end

    def warn_invalid_slideshow_format
      window = Gtk::MessageDialog.new(parent: self,
                                      flags: :destroy_with_parent,
                                      type: :error,
                                      buttons_type: :close,
                                      message: 'Selected file is not a supported slideshow format.')

      window.signal_connect "response" do |dialog, _response_id|
        dialog.destroy
      end

      window.show_all
    end

    def populate_tree
      @bg.items.each_with_index do |item, index|
        iter = @model.append
        item.iter = iter
        if item.type == GWSC::WALLPAPER
          iter.set_values([(index + 1).to_s, WALLPAPER, GdkPixbuf::Pixbuf.new(file: item.file, height: 135, width: 240), GWSC::HumanReadable.secs_to_human(item.duration.to_i)])
        else
          iter.set_values([(index + 1).to_s, TRANSITION, nil, GWSC::HumanReadable.secs_to_human(item.duration.to_i)])
        end
      end
    end

    def set_all_sensitive(val)
      imageFileChooser.set_sensitive(val)
      moveUpButton.set_sensitive(val)
      moveDownButton.set_sensitive(val)
      removeItemButton.set_sensitive(val)
      durationValue.set_sensitive(val)
      durationUnitCombo.set_sensitive(val)
    end
  end
end
