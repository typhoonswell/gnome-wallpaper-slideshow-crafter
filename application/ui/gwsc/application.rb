module GWSC
  class Application < Gtk::Application
    def initialize(*args)
      super 'com.typhoonswell.gnome-wallpaper-slideshow-crafter', Gio::ApplicationFlags::FLAGS_NONE
      signal_connect :activate do |application|
        window = GWSC::ApplicationWindow.new(application, args[0])
        window.present
      end
    end
  end
end
